package com.xxl.job.admin.core.conf;

import com.xxl.job.admin.core.schedule.XxlJobDynamicScheduler;
import org.quartz.Scheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

/**
*
* @date 2018/07/20
* @author caifan 
*
*/
@Configuration
public class QuartzJobConfig {
    @Autowired
    DataSource dataSource;

    @Bean(name = "quartzScheduler")
    @Primary
    public SchedulerFactoryBean setSchedulerFactoryBean(@Qualifier("dataSource") DataSource dataSource) throws IOException {
        SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
        schedulerFactoryBean.setDataSource(dataSource);
        schedulerFactoryBean.setAutoStartup(true);//自动启动
        schedulerFactoryBean.setStartupDelay(20);//系统启动20s后启动
        schedulerFactoryBean.setOverwriteExistingJobs(true);//覆盖DB中JOB：true、以数据库中已经存在的为准：false
        schedulerFactoryBean.setApplicationContextSchedulerContextKey("applicationContextKey");
        schedulerFactoryBean.setQuartzProperties(quartzProperties());
        return schedulerFactoryBean;
    }

    @Bean
    public Properties quartzProperties() throws IOException {
        PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
        propertiesFactoryBean.setLocation(new ClassPathResource("quartz.properties"));
        propertiesFactoryBean.afterPropertiesSet();
        return propertiesFactoryBean.getObject();
    }

   @Bean(name = "xxlJobDynamicScheduler", initMethod = "init", destroyMethod = "destroy")
    public XxlJobDynamicScheduler scheduler(@Qualifier("quartzScheduler")Scheduler scheduler) {
        XxlJobDynamicScheduler xxlJobDynamicScheduler = new XxlJobDynamicScheduler();
        xxlJobDynamicScheduler.setScheduler(scheduler);
        return xxlJobDynamicScheduler;
    }

}
